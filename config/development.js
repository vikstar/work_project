exports.app = app = {
  title: 'node-boot',
  host: 'localhost',
  port: 80,
  ssl: false,
  mongodb_connection_url: 'mongodb://localhost:27017/',
  db_name: 'shop'

// add other config variables here
}