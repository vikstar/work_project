var mbClient = require("mongodb").MongoClient;

var state = {
	db		: null,
	database: null
}

exports.connect = function(url, dbname, callback) {
	if (state.db) return state.db;

	mbClient.connect(url, (err, database) => {
		if (err) return callback(err);
		state.database 	= database;
		state.db 		= database.db(dbname);
		callback();
	});
}

exports.get = function() {
  return state.db;
}

exports.close = function() {
	if (state.db) {
		state.database.close((err, result) => {
			state.database = null;
			state.db = null;
			state.mode = null;
		});
	};
}