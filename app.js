// Module dependencies.
const express = require("express")
  , engine 	= require('ejs-locals') 
  , config 	= require('./config')
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes")
  , db		= require('./db');

var protocol = config.app.ssl ? 'https' : 'http'
	, port = process.env.PORT || config.app.port
	, app_url = protocol + '://' + config.app.host + ':' + port
	, env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';

const app     = express();

// All environments
app.engine('ejs', engine);
app.set("env", env);
app.set("port", port);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

// App routes
app.get("/"			 						 , routes.index);
app.get("/category"							 , routes.categories);
app.get("/category/subcategory"				 , routes.categories);
app.get("/category/subcategory/products"	 , routes.products);
app.get("/category/subcategory/products/item", routes.pdp);

// Connect to Mongo on start
db.connect(config.app.mongodb_connection_url, config.app.db_name, (err) => {
	if (err) {
		console.log('Unable to connect to Mongo.')
		process.exit(1)
	} else {
		http.createServer(app).listen(port, () => {
			console.log("Express server listening on port " + port);
		});
	}
});