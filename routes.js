const db 	= require('./db')
	, _ 	= require("underscore");

exports.index = function(req, res) {
	res.render("index", { 
		// Template data
		project_title 	: "shop"
	});
};

exports.categories = function(req, res) {
	console.log(req);
	const helper 	= require("./helpers");
	var collection 	= db.get().collection('categories');

	var view_link = '/category/subcategory';
	var parts = req.query.id.split("-");
	var mc = parts[0]; // main category id

	// check if viewing a subcategory
	var subcategory = 0;
	if (parts.length > 1) {
		subcategory = 1;
		view_link += '/products';
	}

	var breadcrumbs = helper.breadcrumb(req.query.id);

	// there are only rows in categories collection 
	// so we just pick one depending on the main category selected
	collection.find({"id": mc}).toArray(function(err, items) {
		res.render("categories", { 
			// Underscore.js lib
			_ 				: _, 
			
			// Template data
			breadcrumbs		: breadcrumbs,
			subcategory 	: subcategory,
			view_link 		: view_link,
			category_id 	: req.query.id,
			page_title 		: 'Category',
			items 			: items
		});
	});
};

exports.products = function(req, res) {
	const helper = require("./helpers");
	var collection = db.get().collection('products');
	var perPage = 6;
	var page = req.query.page || 1;

	var breadcrumbs = helper.breadcrumb(req.query.id);

	collection.find(
		{"primary_category_id":req.query.id, "type.master":true, "orderable":true},
		{"_id":0,"name":1,"id":1,"image_groups":1,"short_description":1,"price":1,"currency":1}
	).toArray((err, items) => {
		var count = items.length;
		var range = items.slice((perPage * page) - perPage, page * perPage);
		
		helper.setImage(range, 'medium');

		res.render("products", { 
			// Underscore.js lib
			_ 				: _, 
			
			// Template data
			breadcrumbs 	: breadcrumbs,
			category_id 	: req.query.id,
			page_title 		: 'Subcategory',
			items 			: range,
			current 		: page,
			page_count 		: Math.ceil(count / perPage)
		});
	});
};

exports.pdp = function(req, res) {
	const helper 	= require("./helpers")
		, soap 		= require('soap')
		, _ 		= require('underscore')
		, wsdl 		= 'http://infovalutar.ro/curs.asmx?wsdl';
	var collection = db.get().collection('products');

	collection.findOne({"id": req.query.id}, (err, item) => {

		var breadcrumbs = helper.breadcrumb(item.primary_category_id,item.id);
		helper.setImage([item], 'large');

		var args = {};
		soap.createClient(wsdl, (err, client) => {
			client.lastdateinserted(args, (err, result) => {
				args = {dt : result.lastdateinsertedResult.toJSON()};
				client.getall(args, (err, r) => {
					var currencies = r.getallResult.diffgram.DocumentElement.Currency;
					var USD = _.find(currencies,{'IDMoneda':"USD"}).Value;

					res.render("pdp", { 
						// Underscore.js lib
						_ 				: _, 
						
						// Template data
						breadcrumbs 	: breadcrumbs,
						currencies		: currencies,
						USD				: USD,
						page_title 		: 'Item',
						item 			: item
					});
				});
			});
		});
	});
};