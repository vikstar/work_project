/**
 * sets image property to the filtered products
 * 
 * @param array items      - filtered items
 * @param string view_type - type of image ['small','medium', 'large']
 */
exports.setImage = function setImage(items, view_type) {
	var _ = require("underscore");

	_.each(items, function(item) {
		_.each(item.image_groups, (img) => {
			if (img.view_type == view_type && img.variation_value == null && item.image == null) {
				item.image = _.first(img.images);
			}
		});
	});
}

/**
 * generates the breadcrumbs for the current page
 * 
 * @param  string id      - category.id
 * @param  string item_id - item.id or null
 * @return array          - breadcrumb parts
 */
exports.breadcrumb = function breadcrumb(id, item_id) {
	var capitalize = require("capitalize");
	var _			= require("underscore");
	const url_parts = ["category","subcategory","products"];

	var breadcrumbs = [];
	var tmp_link = "";
	var tmp_id = "";
	var parts = id.split('-');
	_.each(parts, (part, key) => {
		tmp_link += "/" + url_parts[key];
		tmp_id += (tmp_id) ? "-"+part : part;
		breadcrumbs[key] = {name: capitalize(part), link: tmp_link + '?id=' + tmp_id}
	});

	if (item_id) {
		breadcrumbs[parts.length] = {name: item_id, link: tmp_link + '/item?id=' + item_id}
	}

	return breadcrumbs;
}
